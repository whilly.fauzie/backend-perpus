from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
import base64
import datetime
from flask_cors import CORS, cross_origin

cors_config = {
    'origins': ['http://127.0.0.1:5000'],
    'methods': ['GET', 'POST', 'PUT', 'DELETE']
}

app = Flask(__name__)
cors = CORS(app, resources={
    r"/*": cors_config
})
db = SQLAlchemy(app)

app.config['SECRET_KEY']='secret'
app.config['SQLALCHEMY_DATABASE_URI']='postgresql://postgres:Januari2930@localhost:5432/CRUD_test1'


class User(db.Model):
    user_id = db.Column(db.Integer, primary_key=True, index=True)
    email = db.Column(db.String, nullable=False, unique=True)
    user_name = db.Column(db.String, nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)
    borrow_user = db.relationship('Borrowing', backref='borus', lazy='dynamic')

    def __repr__(self):
        return f'User <{self.email}>'

class Book(db.Model):
    book_code = db.Column(db.Integer, primary_key=True, index=True)
    book_title = db.Column(db.String, nullable=False)
    author = db.Column(db.String(50), nullable=False)
    publisher = db.Column(db.String(50))
    genres = db.Column(db.String(50))
    stock = db.Column(db.Integer, nullable=False)
    borrow_book = db.relationship('Borrowing', backref='borbo', lazy='dynamic')

    def __repr__(self):
        return f'Book <{self.book_title}>'

class Borrowing(db.Model):
    borrowing_id = db.Column(db.Integer, primary_key=True, index=True)
    book_code = db.Column(db.Integer, db.ForeignKey('book.book_code'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    borrow_from_date = db.Column(db.String(25), nullable=False)
    borrow_to_date = db.Column(db.String(25), nullable=False)
    book_status = db.Column(db.Boolean, default=False)
    actual_return_date = db.Column(db.String(25), nullable=False)


@app.route('/users/')
def get_users():
    return jsonify([
        {
            'user_id': user.user_id, 'email': user.email, 'user_name': user.user_name, 'password': user.password
        } for user in User.query.all()
    ])

@app.route('/users/<id>/')
def get_user(id):
    print(id)
    user = User.query.filter_by(user_id=id).first_or_404()
    return {
        'user_id': user.user_id, 'email': user.email, 'user_name': user.user_name, 'password': user.password
    }

@app.route('/users/', methods=['POST'])
def create_user():
    data = request.get_json()
    if not 'user_name' in data or not 'email' in data:
        return jsonify({
            'error': 'Bad Request',
            'message': 'Name or email not given'
        }), 400
    u = User(
            email=data['email'],
            user_name=data['user_name'],
            password=data['password']
        )
    db.session.add(u)
    db.session.commit()
    return {
        'user_id': u.user_id, 'user_name': u.user_name, 'email': u.email
    }, 201

@app.route('/users/<id>/', methods=['PUT'])
def update_user(id):
    data = request.get_json()
    if not 'user_name' in data and not 'email' in data and not 'password' in data:
        return {
            'error': 'Bad Request',
            'message': 'field needs to be present'
        }, 400
    user = User.query.filter_by(user_id=id).first_or_404()
    if 'user_name' in data:
        user.user_name = data['user_name']
    if 'email' in data:
        user.email = data['email']
    if 'password' in data:
        user.password = data['password']
    db.session.commit()
    return jsonify({
        'success': 'data has been changed successfully',
        'user_id': user.user_id, 'user_name': user.user_name, 'email': user.email
    })

@app.route('/users/<id>/', methods=['DELETE'])
def delete_user(id):
    user = User.query.filter_by(user_id=id).first_or_404()
    db.session.delete(user)
    db.session.commit()
    return {
        'success': 'Data deleted successfully'
    }


@app.route('/books/')
def get_books():
    return jsonify([
        {
            'book_code': book.book_code, 'book_title': book.book_title, 'author': book.author,
            'publisher': book.publisher, 'genres': book.genres, 'stock': book.stock
        } for book in Book.query.all()
    ])

@app.route('/books/<id>/')
def get_book(id):
    book = Book.query.filter_by(book_code=id).first_or_404()
    return {
        'book_code': book.book_code, 'book_title': book.book_title, 'author': book.author,
        'publisher': book.publisher, 'genres': book.genres, 'stock': book.stock
    }

@app.route('/books/', methods=['POST'])
def add_book():
    data = request.get_json()
    if not 'book_title' in data:
        return jsonify({
            'error': 'Bad Request',
            'message': 'Book Title not given'
        }), 400
    book = Book(
            book_title = data['book_title'],
            author = data['author'],
            publisher = data['publisher'],
            genres = data['genres'],
            stock = data['stock']
        )
    db.session.add(book)
    db.session.commit()
    return {
        'book_code': book.book_code, 'book_title': book.book_title, 'author': book.author,
        'publisher': book.publisher, 'genres': book.genres, 'stock': book.stock
    }, 201

@app.route('/books/<id>/', methods=['PUT'])
def update_book(id):
    data = request.get_json()
    if not 'book_title' in data and not 'author' in data and not 'publisher' in data and not 'genres' in data and not 'stock' in data:
        return {
            'error': 'Bad Request',
            'message': 'field needs to be present'
        }, 400
    book = Book.query.filter_by(book_code=id).first_or_404()
    if 'book_title' in data:
        book.book_title = data['book_title']
    if 'author' in data:
        book.author = data['author']
    if 'publisher' in data:
        book.publisher = data['publisher']
    if 'genres' in data:
        book.genres = data['genres']
    if 'stock' in data:
        book.stock = data['stock']
    db.session.commit()
    return jsonify({
        'success': 'data has been changed successfully',
        'book_code': book.book_code, 'book_title': book.book_title, 'author': book.author,
        'publisher': book.publisher, 'genres': book.genres, 'stock': book.stock
    })

@app.route('/books/<id>/', methods=['DELETE'])
def delete_book(id):
    book = Book.query.filter_by(book_code=id).first_or_404()
    db.session.delete(book)
    db.session.commit()
    return {
        'success': 'Data deleted successfully'
    }


@app.route('/borrow/')
def get_borrow():
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    user = User.query.filter_by(user_name=planb[0], password=planb[1]).first_or_404()
    if planb[0] in user.user_name and planb[1] in user.password:
        return jsonify([
            {
                'book': {
                    'book code': borrowing.borbo.book_code,
                    'book title': borrowing.borbo.book_title,
                    'author': borrowing.borbo.author,
                    'publisher': borrowing.borbo.publisher
                },
                'borrowing id': borrowing.borrowing_id, 'start date': borrowing.borrow_from_date,
                'end date': borrowing.borrow_to_date,
                'borrower': {
                    'user id': borrowing.borus.user_id,
                    'name': borrowing.borus.user_name,
                    'email': borrowing.borus.email
                }
            } for borrowing in Borrowing.query.all()
        ])

@app.route('/borrow/', methods=['POST'])
def add_borrow():
    data = request.get_json()
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    user = User.query.filter_by(user_name=planb[0], password=planb[1]).first_or_404()
    book = Book.query.filter_by(book_code=data['book_code']).first()
    count1 = Borrowing.query.filter_by(book_status=False, book_code=data['book_code']).count()
    if planb[0] in user.user_name and planb[1] in user.password:
        if not 'user_id' in data:
            return jsonify({
                'error': 'Bad Request',
                'message': 'user id of borrower not given'
            }),400
        if not 'book_code' in data:
            return jsonify({
                'error': 'Bad Request',
                'message': 'book code to borrow not given'
            }),400
        if (not 'borrow_from_date' in data) or (not 'borrow_to_date' in data):
            return jsonify({
                'error': 'Bad Request',
                'message': 'borrow start date and end date not given'
            }),400
        if count1 == book.stock:
            return jsonify({
                'Sorry': 'book is not available'
            }),400
        else:
            borrow = Borrowing(
                    book_code = data['book_code'],
                    user_id = data['user_id'],
                    borrow_from_date = data['borrow_from_date'],
                    borrow_to_date = data['borrow_to_date'],
                    book_status = data['book_status']
                )
            db.session.add(borrow)
            db.session.commit()
            return {
                'borrowing id': borrow.borrowing_id, 'start date': borrow.borrow_from_date,
                'end date': borrow.borrow_to_date,
                'book': {
                    'book code': borrow.borbo.book_code,
                    'book title': borrow.borbo.book_title,
                    'author': borrow.borbo.author,
                    'publisher': borrow.borbo.publisher
                },
                'borrower': {
                    'user id': borrow.borus.user_id,
                    'name': borrow.borus.user_name,
                    'email': borrow.borus.email
                }
            }, 201

@app.route('/borrow/<id>/', methods=['PUT'])
def return_borrow(id):
    data = request.get_json()
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    user = User.query.filter_by(user_name=planb[0], password=planb[1]).first()
    if planb[0] in user.user_name and planb[1] in user.password:
        borrow = Borrowing.query.filter_by(borrowing_id=id).first_or_404()
        if 'book_status' in data:
            borrow.actual_return_date = data['actual_return_date']
            borrow.book_status = data['book_status']
            db.session.commit()
            if borrow.book_status:
                return {'message': 'book return is successful',
                    'borrowing id': borrow.borrowing_id, 'start date': borrow.borrow_from_date,
                    'end date': borrow.borrow_to_date, 'return date': borrow.actual_return_date,
                    'book': {
                        'book code': borrow.borbo.book_code,
                        'book title': borrow.borbo.book_title,
                        'author': borrow.borbo.author,
                        'publisher': borrow.borbo.publisher
                    },
                    'borrower': {
                        'user id': borrow.borus.user_id,
                        'name': borrow.borus.user_name,
                        'email': borrow.borus.email
                    },                   
                }, 201
            else:
                return {'error': 'wrong status'}


@app.route('/borrow/users/<id>/')
def get_borrowbook(id):
    bb = Borrowing.query.filter_by(user_id=id)
    return jsonify([
        {
            'book code': borrow.borbo.book_code,
            'book title': borrow.borbo.book_title,
            'author': borrow.borbo.author,
            'publisher': borrow.borbo.publisher,
            'genres': borrow.borbo.genres
        } for borrow in (bb)
    ])

@app.route('/borrow/books/<id>/')
def get_borrowuser(id):
    bb = Borrowing.query.filter_by(book_code=id)
    return jsonify({'message': 'the book has been borrowed by:'},[
        {   
            'user id': borrow.borus.user_id,
            'user name': borrow.borus.user_name,
            'email': borrow.borus.email
        } for borrow in (bb)
    ])


@app.after_request
def after_request_func(response):
    origin = request.headers.get('Origin')
    if request.method == 'OPTIONS':
        response = make_response()
        response.headers.add('Access-Control-Allow-Credentials', 'true')
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type')
        response.headers.add('Access-Control-Allow-Headers', 'x-csrf-token')
        response.headers.add('Access-Control-Allow-Methods',
                             'GET, POST, OPTIONS, PUT, PATCH, DELETE')
        if origin:
            response.headers.add('Access-Control-Allow-Origin', origin)
    else:
        response.headers.add('Access-Control-Allow-Credentials', 'true')
        if origin:
            response.headers.add('Access-Control-Allow-Origin', origin)

    return response