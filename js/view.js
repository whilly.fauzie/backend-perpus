document.addEventListener('DOMContentLoaded', function () {
    const id = window.location.search.split('=')[1]
    fetch('http://127.0.0.1:5000/users/' + id + '/',{
    method: 'GET'
  })
    .then(response => response.json())
    .then(response => {
        const email = document.querySelector('#view-email')
        email.innerHTML = response.email
        const user_name = document.querySelector('#view-username')
        user_name.innerHTML = response.user_name
        const password = document.querySelector('#view-password')
        password.innerHTML = response.password
    })
});