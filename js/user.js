document.addEventListener('DOMContentLoaded', function() {  
  fetch('http://127.0.0.1:5000/users/',{
    method: 'GET'
  })
    .then(response => response.json())
    .then(response => {
        const userTable = document.querySelector('#userTable');
        const tbody = userTable.querySelector('tbody');
        tbody.innerHTML = '';
        response.forEach(item => {
          const row = createHTMLRow({ 
            user_id: item.user_id, 
            email: item.email,
            user_name: item.user_name,
            password: item.password
          });
          tbody.appendChild(row);
        });
      });
  });