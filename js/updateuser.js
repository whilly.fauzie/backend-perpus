document.addEventListener('DOMContentLoaded', function() {
    const id = window.location.search.split('=')[1]
    fetch('http://127.0.0.1:5000/users/' + id + '/',{
        method: 'GET'
    })
        .then(response => response.json())
        .then(response => {
            let updateEmail = document.querySelector('#updateemail')
            updateEmail.value = response.email
            let updateUser_name = document.querySelector('#updateuser_name')
            updateUser_name.value = response.user_name
            let updatePassword = document.querySelector('#updatepassword')
            updatePassword.value = response.password
        })
        
    const updatebutton = document.querySelector('#update-button')
    updatebutton.addEventListener('click', (e) => {
        e.preventDefault();
        const updateForm = document.querySelector('#updateform');
        let data = new FormData(updateForm);
        let json = JSON.stringify((Object.fromEntries(data)));
        
        fetch('http://127.0.0.1:5000/users/' + id + '/',{
            method: 'PUT',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: json
        })
        .then(response => response.json())
        .then(response => {
            console.log(response)
            window.location.href = 'user.html'
        })
    })
    
})