function createHTMLRow(data) {  
  const row = document.createElement('tr');


  // create button view in every row
  const viewbutton = document.createElement('button');
  const textbutton = document.createTextNode('view');
  viewbutton.appendChild(textbutton)
  viewbutton.onclick = () => {
    window.location.href = 'view.html?id=' + data.user_id
    // <a target="_blank" href='view.html?id=' + data.user_id></a>
  }
  
  const editbutton = document.createElement('button');
  editbutton.innerText = "edit"
  editbutton.onclick = () => {
    window.location.href = 'updateuser.html?id=' + data.user_id
  }


  for(prop in data) {
    const cell = document.createElement('td');
    cell.innerHTML = data[prop];
    
    row.appendChild(cell);
    row.appendChild(viewbutton);
    row.appendChild(editbutton)
  }

  return row;
}


