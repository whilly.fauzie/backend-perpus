from sqlalchemy import create_engine
from sqlalchemy.sql import text
from flask import Flask, request, jsonify

app = Flask(__name__)

conn_str = 'postgresql://postgres:Januari2930@localhost:5432/CRUD_test1'
engine = create_engine(conn_str, echo=False)

@app.route('/user', methods=['GET'])
def get_users():
    all = []
    with engine.connect() as connection:
        qry = text("SELECT * FROM public.user ORDER BY user_id")
        result = connection.execute(qry)
        for item in result:
            all.append({
                'user_id':item(0), 'user_name':item(1), 'email':item(2), 'password':item(3)
            })
        return jsonify(all)


# if __name__ == '__main__':
#     app.run()